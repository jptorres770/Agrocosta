

ALTER VIEW [dbo].[TAB_BANCOS]
AS
select
T0.BankCode as IDBanco,
T0.BankName as NombreBanco,
T1.IBAN as Cuenta    
from ODSC T0 
INNER JOIN DSC1 T1 ON T0.BankCode = T1.BankCode AND T0.DfltAcct = T1.Account 
WHERE T1.BankCode <> '9999'
AND ISNULL(T0.DfltAcct,'') <> ''
GROUP BY T0.BankCode, T0.BankName, T1.IBAN 
UNION ALL
SELECT
'0',
'-Envio a central-',
'-Envio a central-'

GO


