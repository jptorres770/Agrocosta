

CREATE VIEW [dbo].[TAB_Articulo]
AS
select
[ItemCode] as ItemCode
   ,a1.[ItemName] as ItemDesc
   ,a1.[ItemCode] + '.jpg' as RutaFoto
   ,a1.[ItemCode] + '.jpg' as RutaFoto
      ,a1.[ListName] as ListaPrecioDefecto
	  ,a1.[CodeBars] as EAN
      ,a1.[ItmsGrpCod] as FamiliaCode
	  ,a1.[U_SEISubFa] as SubFamiliaCode
	  ,a1.[FirmCode] as FabricanteCode
	  ,a1.[SWW] as ProveedorCode
	  ,a1.[InvntryUom] as UnidadInventario
	  ,a1.[SalUnitMsr] as UnidadVenta
	  ,a1.[NumInSale] as CantidadPorUndVenta
      
  
from OITM a1

left join OMRC a2 ON a1.FirmCode = a2.FirmCode
left join OITB a3 ON a1.ItmsGrpCode = a3.ItmsGrpCode
left join [@SEISUBFAMILIA] a4 ON a1.U_SEISubFa = a4.U_SEISubFa


GO

CREATE VIEW [dbo].[TAB_ArticuloPrecio]
AS
select
[ItemCode] as ItemCode
   ,a1.[ItemName] as ItemDesc
   ,a1.[ItemCode] + '.jpg' as RutaFoto
   ,a1.[ItemCode] + '.jpg' as RutaFoto
      ,a1.[ListName] as ListaPrecioDefecto
	  ,a1.[CodeBars] as EAN
      ,a1.[ItmsGrpCod] as FamiliaCode
	  ,a1.[U_SEISubFa] as SubFamiliaCode
	  ,a1.[FirmCode] as FabricanteCode
	  ,a1.[SWW] as ProveedorCode
	  ,a1.[InvntryUom] as UnidadInventario
	  ,a1.[SalUnitMsr] as UnidadVenta
	  ,a1.[NumInSale] as CantidadPorUndVenta
      
  
from OITM a1

left join OMRC a2 ON a1.FirmCode = a2.FirmCode
left join OITB a3 ON a1.ItmsGrpCode = a3.ItmsGrpCode
left join [@SEISUBFAMILIA] a4 ON a1.U_SEISubFa = a4.U_SEISubFa


GO

CREATE VIEW [dbo].[TAB_Familias]
AS
select a2.itmsgrpcod, a2.ItmsGrpNam,a1.FirmCode from oitm a1 left join OITB as a2 on a1.ItmsGrpCod = a2.ItmsGrpCod group by a2.itmsgrpcod, a2.ItmsGrpNam,a1.FirmCode
GO

CREATE VIEW [dbo].[TAB_Banco]
AS
select
[BankCode] as IDBanco
   ,[BankName] as NombreBanco
      
from ODSC

GO

CREATE VIEW [dbo].[TAB_Fabricante]
AS
select
[FirmCode] as Codigo
   ,[FirmName] as Descripcion
      
from ODSC

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_Clientes]
AS

SELECT
	 Cab."CardCode" AS Codigo,
	 Cab."CardName" AS Nombre,
	 ISNULL(Cab."CardFName",
	 Cab."CardName") AS NombreComercial,
	 Cab."CreateDate" AS FechaAlta,

	 ISNULL(Gru."GroupName",
	 '') AS Grupo ,
	 ISNULL(Emp."empID",
	 '') AS EmpID,


	 ISNULL(Cab."Phone1",
	 '') AS Telefono1,
	 ISNULL(Cab."Phone2",
	 '') AS Telefono2,
	 ISNULL(Cab."Cellular",
	 '') AS Telefono2,
	 ISNULL(Cab."E_Mail",
	 '') AS Email,
	 ISNULL(Cab."CntctPrsn",
	 '') AS Contacto,
	 ISNULL(Cab."CreditLine",
	 0) AS LimiteCreditoTotal,

	  ISNULL(Cab."Balance",
	 0) AS CreditoDisponible,







	 REPLACE(ISNULL(Cab."Address",
	 ''),
	 '''',

	 '�') as DireccionFactura ,
	 REPLACE(ISNULL(Cab."City",
	 ''),
	 '''',
	 '�') as PoblacionFactura ,
	 REPLACE(ISNULL(Cab."ZipCode",
	 ''),
	 '''',
	 '�') as CPFactura ,
	 REPLACE(ISNULL(Cab."City",
	 ''),
	 '''',
	 '�') as CiudadFactura ,
	  REPLACE(ISNULL(Cab."County",
	 ''),
	 '''',
	 '�') as ProvinciaFactura ,
	 ISNULL(Lin."U_SEILatitud",
	 0) as Latitud ,
	 ISNULL(Lin."U_SEILongitud",
	 0) as Longitud ,
	 ISNULL(Cab."Groupnum",
	 '') AS CondicionPago,


	 ISNULL(Pag."ListNum",
	 '') AS ListaPrecios,

	 ISNULL(Ban."Account",
	 '') AS NumeroCuenta,
	 FNULL(Cab."Equ",
	 '') AS RecargoEquiv,

	 
	 
	 
	 
	
	 ISNULL(Cab."Free_Text",
	 '') AS Observaciones 
FROM "OCRD" AS Cab 
LEFT JOIN "CRD1" AS Lin ON (Cab."ShipToDef" = Lin."Address") 
AND (Cab."CardCode" = Lin."CardCode") 
AND (Lin."AdresType" = 'S') 
INNER JOIN "OCRG" AS Gru ON Cab."GroupCode" = Gru."GroupCode" 
INNER JOIN "OSLP" AS Slp ON Cab."SlpCode" = Slp."SlpCode" 
LEFT JOIN "OHEM" AS Emp ON Slp."SlpCode" = Emp."salesPrson" 
LEFT OUTER JOIN "OPLN" AS Lis ON Cab."ListNum" = Lis."ListNum" 
LEFT JOIN OCTG AS Pag ON CAB."GroupNum" = Pag.GroupNum
LEFT JOIN OCRB AS Ban ON Ban.CardCode = Cab.CardCode AND Ban.BankCode = Cab.BankCode 
WHERE (Cab."CardType" = 'C') 
AND (Cab."frozenFor" = 'N') 
OR (Cab."CardType" = 'C') 
AND (Cab."validFor" = 'Y') 
ORDER BY Cab."CardCode"

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_FacturasPendCab]
AS

select T0."DocEntry" as DocEntry,
       T0."DocNum" as DocNum,
       isnull(T0."CardCode"
       ,'') as CardCode,

isnull(T0."CardName",'') as CardName,
 
	   isnull(T0."Comments",
       '') as Comentarios,
       T0."DocTotal" as Total,
	   (T0."DocTotal" - isnull(SUM(T1.Paid),0)) as PendienteFra,
       
       isnull(SUM(T1.Paid),0) as Cobrado,T1."Status"
 
FROM "OINV" T0 
INNER JOIN "INV6" T1 ON T0."DocEntry" = T1."DocEntry" 
LEFT JOIN "OCRD" TC ON TC."CardCode" = T0.CardCode 
WHERE T1."Status" = 'O' 
 
AND T1."ObjType" = 13
group by t0.DocEntry, T0.DocNum, T0.CardCode, T0.CardName, T0.Comments, T0.DocTotal,T1."Status"

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_FacturasPendLin]
AS

SELECT
	 
	 T0."DocEntry" as DocEntry,
	 T1."InstlmntID" as IDVto,
	 convert(integer,(convert(varchar,T1."DueDate",112))) as FechaVto,
	 T1."InsTotal" as TotalVto,
	 T1."Paid" as Cobrado
FROM "OINV" T0 
INNER JOIN "INV6" T1 ON T0."DocEntry" = T1."DocEntry" 
INNER JOIN "OHEM" T2 ON T0."SlpCode" = T2."salesPrson" 
WHERE T1."Status" = 'O' 
AND T1."ObjType" = 13
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_HistoricoDocumentosCab]
AS

SELECT
	 Ped."DocEntry" as NumCab,
	 Ped."DocType" as TipoDoc,
	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,
	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 0 as HoraCreacion,
	 ISNULL(Ped."NumAtCard",
	 '') as NumPedidoCliente,
	 Ped.GroupNum as CondicionPago,
	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 Ped."DiscPrcnt" as dtoGralTotal,
	 '' as Entregado,
	'' as Facturado
	 
FROM "ORDR" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "RDR1" Lin on Ped."DocEntry" = Lin."LineTotal"
UNION ALL
SELECT
	 Ped."DocEntry" as NumCab,
	 Ped."DocType" as TipoDoc,
	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,
	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 0 as HoraCreacion,
	 ISNULL(Ped."NumAtCard",
	 '') as NumPedidoCliente,
	 Ped.GroupNum as CondicionPago,
	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 Ped."DiscPrcnt" as dtoGralTotal,
	 '' as Entregado,
	'' as Facturado
	 
FROM "ORDN" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "RDR1" Lin on Ped."DocEntry" = Lin."LineTotal"
UNION ALL
SELECT
	 Ped."DocEntry" as NumCab,
	 Ped."DocType" as TipoDoc,
	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,
	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 0 as HoraCreacion,
	 ISNULL(Ped."NumAtCard",
	 '') as NumPedidoCliente,
	 Ped.GroupNum as CondicionPago,
	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 Ped."DiscPrcnt" as dtoGralTotal,
	 '' as Entregado,
	'' as Facturado
	 
FROM "OINV" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "RDR1" Lin on Ped."DocEntry" = Lin."LineTotal"

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_HistoricoDocumentosLin]
AS
SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "RDR1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "RDN1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "INV1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_DocumentosCab]
AS
SELECT
	 Ped."DocEntry" as NumCab,

	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,

	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 
	 
	 Ped.GroupNum as CondicionPago,

	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 
	 ped.doctotal as TotalBruto,
	 Ped.DocTotal - ped.VatSum as TotalNeto,
	 Ped."DiscPrcnt" as DtoGralPorcent,
	 Ped.VatSum as DtoGralEuros,
	 '' as TotalIVAIncluido,
	 
	 '' as Retenido,
	 ped.EquVatSum as RecargoEquivalencia,
	 
	 '' as Entregado,
	 '' as Facturado,
	 
	 0 as IDDireccion,
	 ped.Address as Direccion,
	 '' as Poblacion,
	 '' as CP,
	 '' as Ciudad,
	 '' as Provincia,
	 
	 ISNULL(Ped."NumAtCard",
	 '') as NumeroPedidoCliente,
	 Ped."DocType" as eTipoDoc
	  
	 
FROM "ORDR" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "RDR1" Lin on Ped."DocEntry" = Lin."LineTotal"
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Ped."DocEntry" as NumCab,

	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,

	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 
	 
	 Ped.GroupNum as CondicionPago,

	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 
	 ped.doctotal as TotalBruto,
	 Ped.DocTotal - ped.VatSum as TotalNeto,
	 Ped."DiscPrcnt" as DtoGralPorcent,
	 Ped.VatSum as DtoGralEuros,
	 '' as TotalIVAIncluido,
	 
	 '' as Retenido,
	 ped.EquVatSum as RecargoEquivalencia,
	 
	 '' as Entregado,
	 '' as Facturado,
	 
	 0 as IDDireccion,
	 ped.Address as Direccion,
	 '' as Poblacion,
	 '' as CP,
	 '' as Ciudad,
	 '' as Provincia,
	 
	 ISNULL(Ped."NumAtCard",
	 '') as NumeroPedidoCliente,
	 Ped."DocType" as eTipoDoc
	  
	 
FROM "ORDN" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "RDN1" Lin on Ped."DocEntry" = Lin."LineTotal"
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Ped."DocEntry" as NumCab,

	 Ped."CardCode" as CardCode,
	 Ped."CardName" as CardName,
	 convert(integer,(convert(varchar,Ped."DocDate",112))) as DocDate,
	 Emp."empID" as EmpID,

	 convert(integer,(convert(varchar,Ped."DocDueDate",112))) as FechaEntrega,
	 Ped."DocNum" as DocNumSAP,
	 isnull(Ped.U_SEIlongitud,'') as Longitud,
	 isnull(Ped.U_SEIlatitud,'') as Latitud,
	 isnull(Ped."Comments",
	 '') as Comentarios,
	 
	 
	 Ped.GroupNum as CondicionPago,

	 SUM(Lin."LineTotal") as TotalLineas,
	 Ped.VatSum as TotalTasas,
	 
	 ped.doctotal as TotalBruto,
	 Ped.DocTotal - ped.VatSum as TotalNeto,
	 Ped."DiscPrcnt" as DtoGralPorcent,
	 Ped.VatSum as DtoGralEuros,
	 '' as TotalIVAIncluido,
	 
	 '' as Retenido,
	 ped.EquVatSum as RecargoEquivalencia,
	 
	 '' as Entregado,
	 '' as Facturado,
	 
	 0 as IDDireccion,
	 ped.Address as Direccion,
	 '' as Poblacion,
	 '' as CP,
	 '' as Ciudad,
	 '' as Provincia,
	 
	 ISNULL(Ped."NumAtCard",
	 '') as NumeroPedidoCliente,
	 Ped."DocType" as eTipoDoc
	  
	 
FROM "INV1" Ped 
INNER JOIN "OSLP" Slp on Ped."SlpCode" = Slp."SlpCode" 
INNER JOIN "OHEM" Emp on Slp."SlpCode" = Emp."salesPrson" 
inner join "INV1" Lin on Ped."DocEntry" = Lin."LineTotal"
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup



GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_DocumentosLin]
AS

SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 Lin."U_SEIDesc1" as dto1,
	 Lin."U_SEIDesc2" as dto2,
	 Lin."U_SEIDesc3" as dto3,
	 Lin."U_SEIDesc4" as dto4,
	 Lin."U_SEIDesc5" as dto5,
	 Lin."DiscPrcnt" as dtoTotal,
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "RDR1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 Lin."U_SEIDesc1" as dto1,
	 Lin."U_SEIDesc2" as dto2,
	 Lin."U_SEIDesc3" as dto3,
	 Lin."U_SEIDesc4" as dto4,
	 Lin."U_SEIDesc5" as dto5,
	 Lin."DiscPrcnt" as dtoTotal,
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "RDN1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup
UNION ALL
SELECT
	 Lin."DocEntry" as NumCab,
	 Lin."LineNum" as NumLin,
	 Lin."ItemCode" as ItemCode,
	 Lin."Dscription" as ItemDesc,
	 Lin."Quantity" as Cantidad,
	 Lin."Price" as Precio,
	 CASE Lin."UseBaseUn" WHEN 'Y' 
THEN 'S' 
ELSE 'N' 
END as EsUnidadInventario,
	 Lin."U_SEIDesc1" as dto1,
	 Lin."U_SEIDesc2" as dto2,
	 Lin."U_SEIDesc3" as dto3,
	 Lin."U_SEIDesc4" as dto4,
	 Lin."U_SEIDesc5" as dto5,
	 Lin."DiscPrcnt" as dtoTotal,
	 Lin."U_SEIDescL" as CodigoDescuento,
	  Lin.Quantity * PriceBefDi as TotalSinDescuentos,
	 Lin.LineTotal as TotalConDescuentos,
	 Vat.Rate as IVAPorcentajeLinea,
	 vat.EquVatPr as IVARecargoPorcentajeLinea,
	  '' as CodTasa1,
	  0 as ImporteTasa1,
	  '' as CodTasa2,
	  0 as ImporteTasa2,
	  '' as CodTasa3,
	  0 as ImporteTasa3,
	  '' as esLineaNueva
	   
FROM "INV1" Lin 
LEFT JOIN OVTG Vat ON Vat.Code = Lin.VatGroup

GO
/****** Object:  View [dbo].[TAB_Usuarios]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[TAB_Usuarios]
AS

SELECT
Emp.empID as EmpID,
ISNULL(Emp.firstName, '') + ' ' + ISNULL(Emp.middleName, '') + ' ' + ISNULL(Emp.lastName,'') AS Name,
ISNULL(Emp.U_SEIPassword,'') AS Pass,
ISNULL(Emp.U_SEISuper, 'N') as SuperUser,
ISNULL(userId,0) as SAPUser,
0 as SumPedidosSAPMensual,
0 as SumPedidosSAPDia,
0 as SumImportePedidosSAPDia,
0 as SumCobrosSAPDia,
0 as SumImporteCobrosSAPDia,

'' as AlmacenCentral,
'' as AlmacenUsuario,
'' as ListaPrecioCompra
FROM OHEM AS Emp



GO

/****** Object:  View [dbo].[TAB_Actividades]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_Actividades]
AS
SELECT
Act.ClgCode as ActivityID,
Act.CardCode as CardCode,
Cli.CardName as CardName1,
Cli.CardFName as CardName2,
ISNULL(Act.Details,'') as Details,
ISNULL(Act.Notes,'') as Notes,
ISNULL(Act.Status,-1) as EstadoID,
ISNULL(Tipo.Code,-1) as TipoID,
ISNULL(Asunto.Code,-1) as AsuntoID,
ISNULL(Act.DocType,-1) as DocRelacionadoTipo,
ISNULL(Act.DocNum,-1) as DocRelacionadoDocEntry,
RIGHT('0000' + CAST(year(Act.Recontact) as varchar(4)),4) + RIGHT('00' + CAST(Month(Act.Recontact) as varchar(4)),2) + RIGHT('00' + CAST(Day(Act.Recontact)as varchar(4)),2) as PlannedStartDate,
RIGHT('0000' + CAST(Act.BeginTime as varchar(4)),4) as PlannedStartTime,
Act.AttendUser as AssignedToSAPUser,
ISNULL(Act.U_SEI_Hotel,0) as Hotel,
ISNULL(Act.U_SEI_Avion,0) as Avion,
ISNULL(Act.U_SEI_Tren,0) as Tren,
ISNULL(Act.U_SEI_Taxi,0) as Taxi,
ISNULL(Act.U_SEI_Kms,0) as Kilometros,
ISNULL(Act.U_SEI_Gasol,0) as Gasolina,
ISNULL(Act.U_SEI_Peaje,0) as Peaje,
ISNULL(Act.U_SEI_Parking,0) as Parking,
ISNULL(Act.U_SEI_Varios,0) as Varios,
ISNULL(Act.U_SEI_MicroTarjeta,0) as Micro_tarjeta,
ISNULL(Act.U_SEI_MicroTalon,0) as Micro_talon,
ISNULL(Act.U_SEI_MicroEfectivo,0) as Micro_efectivo,
ISNULL(Act.U_SEI_FraAdjunto,'N') as FacturaAdjunto,
ISNULL(Act.U_SEI_FraSolicitado,'N') as FacturaSolicitado,
Usr.EmpID as EmpID
FROM OCLG Act
INNER JOIN OCRD Cli on Act.CardCode = Cli.CardCode
INNER JOIN TAB_ActividadesStatus Est ON Act.status = Est.statusID
INNER JOIN TAB_Usuarios Usr ON Act.AttendUser = Usr.SAPUser
INNER JOIN OCLT Tipo on Act.CntctType = Tipo.Code
INNER JOIN OCLS Asunto on Act.CntctSbjct = Asunto.Code
WHERE
ABS(DATEDIFF(MONTH,Act.SeStartDat,GETDATE())) <= 3  --3 meses


GO

/****** Object:  View [dbo].[TAB_Clientes]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[TAB_Clientes] AS
SELECT
Cab.CardCode AS CardCode,
ISNULL(Cab.CardName,'') as CardName1,
ISNULL(Cab.CardFName, Cab.CardName) AS CardName2,
ISNULL(Cab.CntctPrsn,'') as Contact,
ISNULL(Cab.E_Mail,'') as Email,
ISNULL(Cab.LicTradNum,'') as FiscalNumber,
ISNULL(Cab.Notes,'') as Comments,
ISNULL(Cab.Phone1, '') AS Phone1,
ISNULL(Cab.Phone2, '') AS Phone2,
ISNULL(Cab.IntrntSite, '') AS Web,
ISNULL(Cab.Address,'') AS Address,
ISNULL(Cab.ZipCode, '') AS ZipCode,
ISNULL(Cab.City,'') as City,
ISNULL(Cab.Country,'') as Country,
ISNULL(Prov.Name,'') as County,
ISNULL(Gru.GroupName,'') as TipoIC,
ISNULL(Emp.empID,'') as EmpID
FROM OCRD AS Cab
INNER JOIN CRD1 Dir ON Cab.CardCode = Dir.CardCode and Dir.AdresType = 'B'
INNER JOIN [@SEIPROVINCIAS] Prov ON Dir.State = Prov.Code
INNER JOIN OTER Ter ON Prov.U_SEIIdTerritorio = Ter.territryID
INNER JOIN OCRG Gru ON Cab.GroupCode = Gru.GroupCode
INNER JOIN OHEM Emp ON Ter.descript = Emp.jobTitle OR ISNULL(Emp.U_SEISuper, 'N') = 'S'
WHERE
Gru.GroupCode IN ('100','102','103','105','107','108') --Solo los indicados



GO

/****** Object:  View [dbo].[TAB_ClientesContactos]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[TAB_ClientesContactos]
AS
select
Con.cntctCode as Orden,
Con.CardCode as Cliente,
ISNULL(Con.Position,'') as Puesto,
ISNULL(Con.Name,'') as Nombre,
ISNULL(Con.E_MailL,'') as Email,
ISNULL(Con.Tel1,'') as Telefono,
ISNULL(Vis.EmpID,'0') as EmpID
FROM ocpr Con
INNER JOIN OCRD Cli on Con.CardCode = Cli.CardCode
INNER JOIN TAB_Clientes Vis ON VIS.CardCode = Cli.CardCode




GO

/****** Object:  View [dbo].[TAB_PedidosHistoricosCab]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_PedidosHistoricosCab]
AS
SELECT
Ped.DocNum as DocNum,
Ped.DocEntry as DocEntry,
Ped.CardCode as CardCode,
Ped.CardName as CardName,
RIGHT('0000' + CAST(year(Ped.DocDate) as varchar(4)),4) + RIGHT('00' + CAST(Month(Ped.DocDate) as varchar(4)),2) + RIGHT('00' + CAST(Day(Ped.DocDate)as varchar(4)),2) as DocDate,
ISNULL(Ped.Comments,'') as Comentarios,
RIGHT('0000' + CAST(Ped.DocTime as varchar(4)),4) as HoraCreacion,
Ped.DocTotal - Ped.DiscSum as TotalBruto,
Ped.DocTotal as TotalNeto,
ISNULL(Ped.DiscSum,0) as dtoGralTotalEuros,
ISNULL(Ped.DiscPrcnt,0) as dtoGralTotalPercent,
Emp.empID as EmpID
FROM ORDR Ped
INNER JOIN OSLP Slp on Ped.SlpCode = Slp.SlpCode
INNER JOIN OHEM Emp on Slp.SlpCode = Emp.salesPrson
WHERE DATEDIFF(YEAR,Ped.DocDate,GETDATE()) < 24
	


GO

/****** Object:  View [dbo].[TAB_PedidosHistoricosLin]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





	CREATE VIEW [dbo].[TAB_PedidosHistoricosLin]
	AS
	SELECT 
	Cab.DocEntry as DocEntry,
	Lin.LineNum as LineNum,
	Lin.ItemCode as ItemCode,
	Lin.Dscription as ItemDesc,
	Lin.Quantity as Cantidad,
	Lin.Price as PrecioVenta,
	ISNULL(Lin.unitMsr,'') as UnidadInventario,
	Lin.PriceBefDi - Lin.Price as DescuentoEuros,
	Lin.DiscPrcnt as DescuentoPercent,
	PriceBefDi * Quantity as BrutoLinea,
	Lin.LineTotal as NetoLinea,	
	Cab.EmpID as EmpID	
	FROM RDR1 Lin
	INNER JOIN TAB_PedidosHistoricosCab Cab on Cab.DocEntry = Lin.DocEntry
	


GO

/****** Object:  View [dbo].[TAB_ClientesDirecciones]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[TAB_ClientesDirecciones]
AS
select 
Vis.CardCode as Cliente,
ISNULL(Dir.Address,'') as IDDireccion,
ISNULL(Dir.Street,'') as Direccion,
ISNULL(Dir.County,'') as Poblacion,
ISNULL(Dir.City,'') as Ciudad,
ISNULL(Dir.ZipCode,'') as CP,
ISNULL(Vis.County,'') as Provincia,
ISNULL(Dir.AdresType,'') as TipoDireccion,
ISNULL(Dir.Address,'') as NombreDireccion,
ISNULL(U_SEILat,'') as Latitud,
ISNULL(U_SEILon,'') as Longitud,
Vis.EmpID as EmpID
FROM CRD1 Dir 
INNER JOIN TAB_Clientes Vis ON Dir.CardCode = Vis.CardCode




GO

/****** Object:  View [dbo].[TAB_ActividadesAsuntos]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_ActividadesAsuntos]
AS
select 
T1.Code as TipoID,
T0.Code as AsuntoID, 
T0.Name as Descripcion,
CASE T0.Code WHEN 7 THEN 'S' ELSE 'N' END as PorDefecto
from OCLS T0 
INNER JOIN OCLT T1 ON T0.Type = T1.Code
GO

/****** Object:  View [dbo].[TAB_ClientesCentros]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_ClientesCentros]
AS 
SELECT Code as Cliente,
ISNULL(U_SEIProveedor,'') as ClienteAsociado
FROM [@SEILRELACION] T0 
INNER JOIN OCRD T1 ON T0.U_SEIProveedor = T1.CardCode


GO

/****** Object:  View [dbo].[TAB_ActividadesTipos]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_ActividadesTipos]
AS
select 
Code as TipoID, 
Name as Descripcion,
CASE Code WHEN -1 THEN 'S' ELSE 'N' END as PorDefecto
from OCLT




GO

/****** Object:  View [dbo].[TAB_MensajesBajada]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TAB_MensajesBajada]
AS
SELECT   
C.Code, 
C.Priority, 
rtrim(replace(replace(replace(cast(C.Subject as varchar(100)),CHAR(10),''),CHAR(13),' '),CHAR(9),' ')) as Subject, 
rtrim(replace(replace(replace(cast(C.UserText as varchar(160)),CHAR(10),''),CHAR(13),' '),CHAR(9),' '))  as UserText, 
isnull(L.WasRead,'') as WasRead,
Emp.empID as EmpId, 
ISNULL(U.USER_CODE, '') AS De,
isnull(L.Deleted,'') as Eliminado
FROM OALR AS C 
INNER JOIN dbo.OAIB AS L ON C.Code = L.AlertCode 
INNER JOIN dbo.OUSR AS U ON U.USERID = C.UserSign
INNER JOIN OHEM Emp ON Emp.USERID = L.UserSign

GO

/****** Object:  View [dbo].[TAB_CONSULTA_PARTOS_PREVISTOS]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[TAB_CONSULTA_PARTOS_PREVISTOS]
AS

SELECT Pedido.DocNum as Pedido,
ISNULL(Pedido.U_SEI_IDDispo,'') as Dispositivo,
RIGHT('0000' + CAST(year(ISNULL(Pedido.U_SEI_FEstim,'20000101')) as varchar(4)),4) + RIGHT('00' + CAST(Month(ISNULL(Pedido.U_SEI_FEstim,'20000101')) as varchar(4)),2) + RIGHT('00' + CAST(Day(ISNULL(Pedido.U_SEI_FEstim,'20000101'))as varchar(4)),2) as FechaParto,
Pedido.CardName NombreMadre,
ISNULL(Pedido.U_SEINPrescriptor,'') as Prescriptor,
ISNULL(Pedido.U_SEI_NAseg,'') as  Aseguradora,
ISNULL(Pedido.U_SEI_NESegui,'') as Matrona,
ISNULL(Pedido.U_SEI_NMSegui,'') as Medico,
ISNULL(Provincia.Name,'') as Provincia,
ISNULL(Empleado.empID,0) as EmpID,
ISNULL(Empleado.firstName, '') + ' ' + ISNULL(Empleado.middleName, '') + ' ' +  ISNULL(Empleado.lastName, '') as EmpNombre,
ISNULL(Ter.descript,'') as Zona
FROM ORDR Pedido 
INNER JOIN CRD1 Direccion ON Pedido.CardCode = Direccion.CardCode
INNER JOIN [@SEIPROVINCIAS] Provincia ON Direccion.State = Provincia.Code
INNER JOIN OTER Ter ON Provincia.U_SEIIdTerritorio = Ter.territryID
INNER JOIN OHEM Empleado ON Empleado.salesPrson = Pedido.SlpCode
WHERE
ISNULL(Pedido.U_SEI_FEstim, '20000101') >= GETDATE()
GROUP BY 
Pedido.DocNum, 
Pedido.U_SEI_IDDispo, Pedido.U_SEI_FEstim, Pedido.CardName, Pedido.U_SEINPrescriptor, Pedido.U_SEI_NAseg, Pedido.U_SEI_NAseg, Pedido.U_SEI_NESegui, Pedido.U_SEI_NMSegui, Provincia.Name, Empleado.empID,
ISNULL(Empleado.firstName, '') + ' ' + ISNULL(Empleado.middleName, '') + ' ' +  ISNULL(Empleado.lastName, ''), ISNULL(Ter.descript,'')


GO

/****** Object:  View [dbo].[TAB_CONSULTA_PARTOS_ASIGNADOS]    Script Date: 25/06/2015 10:52:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[TAB_CONSULTA_PARTOS_ASIGNADOS] AS
SELECT        
Pedido.DocNum AS Pedido, 
ISNULL(Pedido.U_SEI_IDDispo, '') AS Dispositivo, 
RIGHT('0000' + CAST(YEAR(ISNULL(Pedido.U_SEI_FEstim, '20000101')) AS varchar(4)), 4) + RIGHT('00' + CAST(MONTH(ISNULL(Pedido.U_SEI_FEstim, '20000101')) AS varchar(4)), 2) + RIGHT('00' + CAST(DAY(ISNULL(Pedido.U_SEI_FEstim, '20000101')) AS varchar(4)), 2) AS FechaParto, 
Pedido.CardName AS NombreMadre, 
ISNULL(Canales.Name, '') AS Canal, 
ISNULL(Medios.Name, '') AS Medio, 
ISNULL(Prescriptor.Name, '') AS TipoPrescriptor, 
ISNULL(Pedido.U_SEINPrescriptor, '') AS Prescriptor, 
ISNULL(Hospital.CardCode, '') AS IDHospital, 
ISNULL(Pedido.U_SEI_NHDefin, N'') AS Hospital, 
ISNULL(Pedido.U_SEI_NAseg, N'') AS Aseguradora,
ISNULL(Pedido.U_SEI_NESegui, N'') AS MatronaSeguimiento, 
ISNULL(Pedido.U_SEI_NMSegui, N'') AS MedicoSeguimiento, 
ISNULL(Pedido.U_SEI_NMAten, N'') AS MedicoParto, 
ISNULL(Pedido.U_SEI_NEAten, N'') AS MatronaParto, 
ISNULL(Provincia.Name, N'') AS Provincia, 
ISNULL(Empleado.empID, 0) AS EmpID,
ISNULL(Empleado.firstName, '') + ' ' + ISNULL(Empleado.middleName, '') + ' ' +  ISNULL(Empleado.lastName, '')  AS EmpNombre,
ISNULL(Ter.descript,'') as Zona
FROM ORDR AS Pedido 
INNER JOIN CRD1 AS Direccion ON Pedido.CardCode = Direccion.CardCode 
INNER JOIN [@SEIPROVINCIAS] Provincia ON Direccion.State = Provincia.Code
INNER JOIN OTER Ter ON Provincia.U_SEIIdTerritorio = Ter.territryID
INNER JOIN OHEM AS Empleado ON Empleado.salesPrson = Pedido.SlpCode 
INNER JOIN OCRD AS Hospital ON Pedido.U_SEI_HDefin = Hospital.CardCode 

LEFT OUTER JOIN [@SEIRESCRIOTOR] AS Prescriptor ON Pedido.U_SEIPrescriptor = Prescriptor.Code 
LEFT OUTER JOIN [@SEIMEDIOS] AS Medios ON Pedido.U_SEIMedio = Medios.Code 
LEFT OUTER JOIN [@SEICANALES] AS Canales ON Pedido.U_SEICanal = Canales.Code
WHERE 1=1 
AND DATEDIFF(DD, ISNULL(Pedido.U_SEI_FEstim, '20000101'), GETDATE()) < 40  --Ultimos 40 dias.
GROUP BY DocNum, Pedido.U_SEI_IDDispo, Pedido.U_SEI_FEstim, Pedido.CardName, Canales.Name, Medios.Name, Prescriptor.Name, Pedido.U_SEINPrescriptor, Hospital.CardCode, 
Pedido.U_SEI_NHDefin, Pedido.U_SEI_NAseg, Pedido.U_SEI_NESegui, Pedido.U_SEI_NMSegui, Pedido.U_SEI_NMAten, Pedido.U_SEI_NEAten, Provincia.Name, Empleado.empID, 
ISNULL(Empleado.firstName, '') + ' ' + ISNULL(Empleado.middleName, '') + ' ' +  ISNULL(Empleado.lastName, '') , Ter.descript


GO

