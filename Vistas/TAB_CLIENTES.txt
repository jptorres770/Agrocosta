

ALTER VIEW [dbo].[TAB_CLIENTES]
AS
SELECT 
	  Cab.SlpCode as Test,
	 Cab.CardCode AS Codigo,
	 REPLACE(REPLACE(REPLACE(REPLACE(Cab.CardName , '''', '�'), '"', ''), '<', ''), '>', '')AS Nombre,
	 REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(Cab.CardFName, Cab.CardName) , '''', '�'), '"', ''), '<', ''), '>', '') AS NombreComercial,
	 CONVERT(char(10), ISNULL(Cab.U_SEIFALT,'20000101'), 112) AS FechaAlta,
	 ISNULL(Cab.U_SEIsegme, '') AS Grupo,	
	 REPLACE(ISNULL(Cab.LicTradNum, ''),'ES','') AS NIF,	 
	 ISNULL(Emp.empID, '') AS EmpID,
	 CASE WHEN ISNULL(Cab.frozenFor, 'N') <> 'N' THEN 'S' ELSE ISNULL(Cab.frozenFor, 'N') END as Congelado,
	 ISNULL(Cab.Phone1, '') AS Telefono1,
	 ISNULL(Cab.Phone2, '') AS Telefono2,
	 ISNULL(Cab.Cellular, '') AS TelefonoMv,
	 ISNULL(Cab.E_Mail, '') AS Email,
	 ISNULL(Cab.CntctPrsn, '') AS Contacto,	 	 
	 -- CREDITO (Teniendo en cuenta consolidacion)--
	 ISNULL(Cab.CreditLine, 0) AS LimiteCreditoTotal,
	 CASE WHEN ISNULL(Cab.FatherCard ,'')='' THEN Cab.Balance + Cab.DNotesBal+ Cab.OrdersBal
	 ELSE (SELECT SUM(Pad.Balance + Pad.DNotesBal + Pad.OrdersBal) From OCRD Pad WHERE Cab.FatherCard= Pad.FatherCard) END as CreditoConsumido,
	 ISNULL(Cab.CreditLine, 0) - CASE WHEN ISNULL(Cab.FatherCard ,'')='' THEN Cab.Balance + Cab.DNotesBal+ Cab.OrdersBal
	 ELSE (SELECT SUM(Pad.Balance + Pad.DNotesBal + Pad.OrdersBal) From OCRD Pad WHERE Cab.FatherCard= Pad.FatherCard) END as CreditoDisponible,
	 -- FIN CREDITO --
	 ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = Cab.Cardcode AND P.DocDate BETWEEN DATEADD(YEAR, -1, GETDATE()) AND GetDate()) ,0) as VentasUltAnyo , 
	 ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = Cab.Cardcode AND P.DocDate BETWEEN DATEADD(YEAR, -2, GETDATE()) AND DATEADD(YEAR, -1, GETDATE())) ,0) as VentasAnyoAnt,  
	 --ISNULL((SELECT SUM(F.DocTotal) FROM OINV F WHERE F.CardCode = Cab.Cardcode AND F.DocDate BETWEEN DATEADD(Year,-1, GetDate()) AND GetDate()) ,0) as VentasUltAnyo, 
	 --ISNULL((SELECT SUM(F.DocTotal) FROM OINV F WHERE F.CardCode = Cab.Cardcode AND YEAR(F.DocDate) = Year(GetDate())-1 ),0) as VentasAnyoAnt, 
	 CASE WHEN ISNULL(Cab.U_SEIopera, '') = '03' THEN 'S' ELSE 'N' END AS GestionSAC,
	 ISNULL((SELECT AVG(P.DocTotal) FROM ORDR P WHERE P.CardCode = Cab.Cardcode),0) as ImpMedioPedido, 
	 CONVERT(char(10), ISNULL((SELECT MAX(A.DocDate) FROM ODLN A WHERE A.CardCode = Cab.Cardcode),'20000101'), 112) as FechaUltAlbaran, 
	 ISNULL(Cab.U_SEIOBSAL, '') AS ObsAlbaran,
	 ISNULL(Cab.U_SEIobstv, '') AS Notas,
	 REPLACE(ISNULL((SELECT TOP 1 DF.Street FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as DireccionFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.City FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as PoblacionFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.ZipCode FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as CPFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 DF.City FROM CRD1 AS DF WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as CiudadFactura ,
	 REPLACE(ISNULL((SELECT TOP 1 PF.Name FROM CRD1 AS DF LEFT JOIN OCST PF ON DF.State = PF.Code WHERE DF.Address=Cab.BillToDef AND DF.CardCode = Cab.CardCode), ''), '''', '�') as ProvinciaFactura ,
	 0 as Latitud ,
	 0 as Longitud ,
	 ISNULL(Pag.PymntGroup, '') AS CondicionPago,
	 ISNULL((SELECT TOP 1 C.PmntDate FROM CRD5 as C WHERE C.CardCode = Cab.CardCode),'') as DiaPago,
	 ISNULL((SELECT TOP 1 Pago.Descript FROM CRD2 AS V INNER JOIN OPYM Pago ON V.PymCode=Pago.PayMethCod WHERE V.CardCode = Cab.CardCode),'') as ViaPago,
	 CASE WHEN ISNULL(Cab.QryGroup2,'N') = 'Y'  THEN 'Fac.Agrupada' 
	 ELSE CASE WHEN ISNULL(Cab.QryGroup3,'N') = 'Y'  THEN 'Fac.Agr.Albar�n' 
	 ELSE CASE WHEN ISNULL(Cab.QryGroup6,'N') = 'Y'  THEN 'Fac.Agr.Dom.Entrega'
	 ELSE CASE WHEN ISNULL(Cab.QryGroup7,'N') = 'Y'  THEN 'Fac.Agr.Quincenal'
	 ELSE '' END  END END END as Facturacion,
	 ISNULL(Pre.ListNum,'') AS ListaPreciosCode,
	 ISNULL(Pre.ListName,'') AS ListaPreciosName,
	 ISNULL((Select TOP 1 AVG(DATEDIFF(DD,Fac.DocDate, CabCobro.DocDate)) as PlazoMedio
			from RCT2 LinCobro 
			INNER JOIN ORCT CabCobro on LinCobro.DocNum=CabCobro.DocEntry
			INNER JOIN OINV Fac on Fac.DocEntry=LinCobro.DocEntry and LinCobro.InvType=13
			WHERE CabCobro.CardCode = Cab.CardCode
			GROUP BY CabCobro.CardCode),0) AS PlazoMedioPago,
	 ISNULL(Ban.Account, '') AS NumeroCuenta,
	 CASE WHEN ISNULL(Cab.Equ, 'N') <> 'N' THEN 'S' ELSE  ISNULL(Cab.Equ, 'N') END AS RecargoEquiv,
     ISNULL(Cab.U_SEICateg,'') AS Categoria,
	 ISNULL(Act.Name, '') AS Actividad,
	 'S' as Sincro, 
	 0 as FechaSincro,
	 '' as IncidenciaSincro
	FROM OCRD AS Cab 	
	LEFT JOIN  [@SEIACTIVIDAD] Act ON Cab.U_SEIActi = Act.Code	
	LEFT JOIN OPLN Pre ON Cab.ListNum = Pre.ListNum
	LEFT JOIN OCTG Pag ON Cab.GroupNum = Pag.GroupNum
	INNER JOIN OHEM AS Emp ON Cab.SlpCode = Emp.salesPrson OR Cab.SlpCode IN (SELECT SEIEmp.U_SEIslphi FROM [@SEIEMPLE] SEIEmp WHERE ISNULL(SEIEmp.U_SEIestad,'') <> '99' AND SEIEmp.U_SEIslppa = Emp.salesPrson)
	LEFT JOIN CRD1 AS Lin ON (Cab.ShipToDef = Lin.Address) AND (Cab.CardCode = Lin.CardCode) AND (Lin.AdresType = 'S') 
	LEFT JOIN OCRB AS Ban ON Ban.CardCode = Cab.CardCode AND Ban.BankCode = Cab.BankCode AND Cab.BankCode = Ban.Account
	WHERE Cab.CardType = 'C'

GO


