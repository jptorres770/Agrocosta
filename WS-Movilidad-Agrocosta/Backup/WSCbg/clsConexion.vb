﻿Imports System.Data.SqlClient
Imports System.Resources

Public Class clsConexion

    Sub New()
        'Constructor
    End Sub

    Public Function OpenConexionSQLSAP() As SqlConnection

        Try
            Dim mCon As SqlConnection = Nothing

            If IsNothing(mCon) Then
                mCon = New SqlConnection()
                mCon.ConnectionString = ConfigurationManager.ConnectionStrings.Item("Conexion").ConnectionString
                mCon.Open()

            ElseIf mCon.State <> ConnectionState.Open Then

                mCon = New SqlConnection()
                mCon.ConnectionString = ConfigurationManager.ConnectionStrings.Item("Conexion").ConnectionString
                mCon.Open()
            End If

            If mCon Is Nothing Then
                Throw New Exception("Conexión nula")
            End If

            If mCon.State <> ConnectionState.Open Then
                Throw New Exception("Conexión cerrada")
            End If

            Return mCon

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw New Exception("Imposible abrir conexión")
        End Try

    End Function

    Public Function OpenConexionSQLTELYNET() As SqlConnection

        Try
            Dim mCon As SqlConnection = Nothing

            If IsNothing(mCon) Then
                mCon = New SqlConnection()
                mCon.ConnectionString = ConfigurationManager.ConnectionStrings.Item("ConexionTelynet").ConnectionString
                mCon.Open()

            ElseIf mCon.State <> ConnectionState.Open Then

                mCon = New SqlConnection()
                mCon.ConnectionString = ConfigurationManager.ConnectionStrings.Item("ConexionTelynet").ConnectionString
                mCon.Open()
            End If

            If mCon Is Nothing Then
                Throw New Exception("Conexión nula")
            End If

            If mCon.State <> ConnectionState.Open Then
                Throw New Exception("Conexión cerrada")
            End If

            Return mCon

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Throw New Exception("Imposible abrir conexión")
        End Try

    End Function

    Public Function ObtenerDS(ByVal SQL As String, ByVal NombreDS As String, ByVal NombreTabla As String) As Data.DataSet

        Dim sqlAdap As Data.SqlClient.SqlDataAdapter
        Dim retVal As New Data.DataSet(NombreDS)
        Dim mCon As SqlConnection = Nothing

        Try

            mCon = New clsConexion().OpenConexionSQLSAP()
            sqlAdap = New Data.SqlClient.SqlDataAdapter(SQL, mCon)

            'Incrementamos el timeout
            sqlAdap.SelectCommand.CommandTimeout = 120

            sqlAdap.Fill(retVal, NombreTabla)


        Catch ex As SqlException

            If ex.Message.Contains("interbloqueo") Or ex.Message.Contains("deadlock") Then

                'Si es una excepcion de interbloqueo no notificar. Es un problema de SQL server + SAP.
                clsLog.Log.Warn(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            ElseIf ex.Number = -2 Then  'Esto es timeout, tampoco notificar.

                clsLog.Log.Warn(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)

            Else

                clsLog.Log.Fatal(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Throw ex

            End If

            Return Nothing

        Catch ex As Exception

            If ex.Message.Contains("interbloqueo") Or ex.Message.Contains("deadlock") Or ex.Message.Contains("Timeout") Then
                clsLog.Log.Warn(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Else
                clsLog.Log.Fatal(ex.Message & vbNewLine & " -- SQL: " & SQL & vbNewLine & " -- en la clase: " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Throw ex
            End If


            Return Nothing
        Finally
            sqlAdap = Nothing
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


End Class
