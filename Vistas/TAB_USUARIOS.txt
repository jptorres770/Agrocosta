

ALTER VIEW [dbo].[TAB_USUARIOS] 
AS
SELECT 
ISNULL(T3.empID, -1) as EmpID, 
ISNULL(T3.firstName,'') + ' ' + ISNULL(T3.middleName,'') + ' ' + ISNULL(T3.lastName,'') as Name, 
ISNULL(T3.U_SEIPass,'') as Pass, 
CASE WHEN ISNULL(U_SEIEmp1,'0') <> '0' THEN 'S' ELSE 
CASE WHEN ISNULL(U_SEIEmp2,'0') <> '0' THEN 'S' ELSE 
'N' END END as SuperUser,
CASE WHEN ISNULL(U_SEIEmp1,'0') <> '0' THEN U_SEIEmp1 ELSE '-1' END as EmpIDExtra1,
CASE WHEN ISNULL(U_SEIEmp2,'0') <> '0' THEN U_SEIEmp2 ELSE '-1' END as EmpIDExtra2,
ISNULL(T3.userId,'0') as SAPUser, 
ISNULL(T3.salesPrson,0) as SlpCode, 
ISNULL(T3.U_SEICamRuta,'N') as CambioRuta, 
ISNULL((SELECT TOP 1 
CASE  MONTH(GETDATE()) 
WHEN 1 THEN U_SEIObjEne 
WHEN 2 THEN U_SEIObjFeb 
WHEN 3 THEN U_SEIObjMar 
WHEN 4 THEN U_SEIObjAbr
WHEN 5 THEN U_SEIObjMay
WHEN 6 THEN U_SEIObjJun
WHEN 7 THEN U_SEIObjJul
WHEN 8 THEN U_SEIObjAgo
WHEN 9 THEN U_SEIObjSep
WHEN 10 THEN U_SEIObjOct
WHEN 11 THEN U_SEIObjNov
WHEN 12 THEN U_SEIObjDic END
FROM [@SEIOBJETIVOSMES] OBJ where OBJ.U_SEIEmpID = T3.empID AND GETDATE() BETWEEN OBJ.U_SEIFechaDesde AND OBJ.U_SEIFechaHasta) , 0) as ObjetivoMensual,
ISNULL((SELECT COUNT(P1.DocEntry) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND P1.docdate = CAST(GETDATE() AS DATE)),0) as  NumeroPedidosSAPDia,
ISNULL((SELECT COUNT(P1.DocEntry) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND MONTH(P1.DocDate) = MONTH(getdate()) AND YEAR(P1.docdate) = YEAR(GETDATE())),0) as  NumeroPedidosSAPMes,
ISNULL((SELECT SUM(P1.DocTotal) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND P1.docdate = CAST(GETDATE() AS DATE)),0) as  ImportePedidosSAPDia,
ISNULL((SELECT SUM(P1.DocTotal) FROM ORDR P1 WHERE P1.SlpCode = T3.salesPrson AND MONTH(P1.DocDate) = MONTH(getdate()) AND YEAR(P1.docdate) = YEAR(GETDATE())),0) as  ImportePedidosSAPMes,
ISNULL((SELECT COUNT(CP.Code) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND CAST(CP.U_SEIFechaCobro as DATE) = CAST(GETDATE() AS DATE)),0) as NumeroCobrosSAPDia,
ISNULL((SELECT COUNT(CP.Code) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND MONTH(CAST(CP.U_SEIFechaCobro as DATE)) = MONTH(CAST(GETDATE() AS DATE)) AND YEAR(CAST(CP.U_SEIFechaCobro as DATE)) = YEAR(CAST(GETDATE() AS DATE))),0) as NumeroCobrosSAPMes,
ISNULL((SELECT SUM(CP.U_SEICobrado) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND CAST(CP.U_SEIFechaCobro as DATE) = CAST(GETDATE() AS DATE)),0) as ImporteCobrosSAPDia,
ISNULL((SELECT SUM(CP.U_SEICobrado) FROM [@COBROSPARCIALES] CP WHERE CP.U_SEIEmpID = T3.empID AND CP.U_SEITipoDoc ='F' AND MONTH(CAST(CP.U_SEIFechaCobro as DATE)) = MONTH(CAST(GETDATE() AS DATE)) AND YEAR(CAST(CP.U_SEIFechaCobro as DATE)) = YEAR(CAST(GETDATE() AS DATE))),0) as ImporteCobrosSAPMes,
'0' as AlmacenCentral,
ISNULL(T1.U_SEIalmac,'') as AlmacenUsuario,
'' as ListaPrecioCompra,
'' as ListaPrecioVenta,
ISNULL(T3.U_SEILocal, 'N') as Localizable,
0 as ObjetivoAcumulado,
ISNULL((SELECT TOP 1 NumAtCard from OINV where slpCode = T0.U_SEIslppa ORDER BY DocDate DESC), '') as NumeracionActualFactura,
ISNULL((SELECT TOP 1 TA.CounterRef FROM ORCT TA 
INNER JOIN OCRD TB ON TA.CardCode = TB.CardCode
INNER JOIN OSLP TC ON TC.SlpCode = TB.SlpCode
WHERE TB.SlpCode = T3.salesPrson
ORDER BY TA.DocNum DESC), '') as NumeracionActualCobro,
ISNULL((SELECT TOP 1 NumAtCard from ORIN where slpCode = T0.U_SEIslppa ORDER BY DocDate DESC), '') as NumeracionActualNotaCredito
from [@SEIEMPLE] T0
LEFT JOIN OSLP T1 ON T0.U_SEIslppa = T1.SlpCode
LEFT JOIN OHEM T3 ON T3.salesPrson = T1.SlpCode
LEFT JOIN OUSR T4 ON T3.userId = T4.USERID
LEFT JOIN OUDG T5 ON T4.DfltsGroup = T5.Code



GO


