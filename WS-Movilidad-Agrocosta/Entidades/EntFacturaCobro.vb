﻿Public Class EntFacturaCobro
    'TODO:comentar tipodoc
    Public Property TipoDoc As String

    Public Property DocEntry As Long
    Public Property IDVto As Long
    Public Property Cobrado As Double

    Public Property IDCobro As Long

    Public Property EmpID As Integer
    Public Property FechaCobro As Integer
    Public Property MedioCobro As String
    Public Property NumTalon As String
    Public Property FechaVtoTalon As Integer

    Public Property CardCode As String
    Public Property CardName As String

    Public Property Sincro As String
    Public Property IncidenciaSincro As String
    Public Property FechaSincro As Integer

End Class
