﻿
Option Explicit On

Imports SAPbobsCOM.BoObjectTypes
Imports WSCbg.SEI_Globals


Public Class SEI_DescuentosDI
    '
    Private sPromocion As String
    '
    Public Structure st_ListaDescuento
        Public c0_Code As String
        Public c1_lDescuento1 As String
        Public c2_lDescuento2 As String
        Public c3_lDescuento3 As String
        Public c4_lDescuento4 As String
        Public c5_lDescuento5 As String
    End Structure
    '
    Public Enum TIPOVENTASoCOMPRAS
        TVentas = 0
        TCompras = 1
    End Enum
    Public Enum USERINTERFACEoDATAINTERFACE
        TUserInterface = 0
        TDataInterface = 1
    End Enum
    Private Enum TipoInterlocutor
        TNingunoInterlocutor = 0
        TCategoria = 1
        TZona = 2
        TActividad = 3
        TInterlocutor = 4
    End Enum
    Private Enum TipoArticulo
        TNingunoArticulo = 0
        TMarca = 1
        TFamilia = 2
        TSubFamilia = 3
        TArticulo = 4
    End Enum
    Private Enum TipoFecha
        TipoSI = 0
        TipoNO = 1
    End Enum
    '
    Const cAplicarDescuento As String = "U_SEI016"
    '
    Private _SessionID As String
    '
    Property sSessionID() As String
        Get
            sSessionID = _SessionID
        End Get
        Set(ByVal value As String)
            _SessionID = value
        End Set
    End Property
    Public Sub New()
    End Sub
    '
    Public Function DESCUENTO_LINIA(ByVal sArticulo As String, _
                                    ByVal sInterlocutor As String, _
                                    ByVal sFecha As String, _
                                    ByVal dCantidad As Double, _
                                    ByVal iTipoDocumento As TIPOVENTASoCOMPRAS,
                                    ByRef oCompany As SAPbobsCOM.Company) As Long
        '
        'A partir d'un client, un article i una data de document, retorna el número de registre (Code) de la
        'taula @SEIDESCUENTOSCLI o @SEIDESCUENTOSPRO que té la condició més restrictiva
        '
        Dim sCategoria As String
        Dim sZona As String
        Dim sActividad As String
        Dim iMarca As Long
        Dim iFamilia As Long
        Dim sSubFamilia As String
        Dim ls As String
        Dim oRecordsetInterlocutor As SAPbobsCOM.Recordset = Nothing
        Dim oRecordsetItem As SAPbobsCOM.Recordset = Nothing
        Dim lDescuento_Linea As Long
        '
        lDescuento_Linea = 0
        DESCUENTO_LINIA = 0
        '
        '-----------------------------------
        ' Datos Interlocutor Comercial
        '-----------------------------------
        ls = ""
        ls = ls & " SELECT CardCode,U_SEICateg,SlpCode,U_SEIActi"
        ls = ls & " FROM OCRD WHERE CardCode = '" & sInterlocutor & "'"
        '
        oRecordsetInterlocutor = oCompany.GetBusinessObject(BoRecordset)
        oRecordsetInterlocutor.DoQuery(ls)
        If oRecordsetInterlocutor.EoF Then
            Throw New Exception("Interlocutor : " & sInterlocutor & " inexistente.")
        End If
        '
        sCategoria = oRecordsetInterlocutor.Fields.Item("U_SEICateg").Value
        sZona = oRecordsetInterlocutor.Fields.Item("SlpCode").Value
        sActividad = oRecordsetInterlocutor.Fields.Item("U_SEIActi").Value
        '
        '-----------------------------------
        ' Datos Articulo
        '-----------------------------------
        '
        ls = ""
        ls = ls & "SELECT ItemCode,FirmCode,ItmsGrpCod,U_SEISubFa  "
        ls = ls & "FROM OITM WHERE ItemCode = '" & sArticulo & "'"
        '
        oRecordsetItem = oCompany.GetBusinessObject(BoRecordset)
        oRecordsetItem.DoQuery(ls)
        If oRecordsetInterlocutor.EoF Then
            Throw New Exception("Artículo : " & sArticulo & " inexistente.")
        End If
        '
        iMarca = oRecordsetItem.Fields.Item("FirmCode").Value
        iFamilia = oRecordsetItem.Fields.Item("ItmsGrpCod").Value
        sSubFamilia = oRecordsetItem.Fields.Item("U_SEISubFa").Value
        '
        Dim sStoredProcedure As String = """"
        If iTipoDocumento = TIPOVENTASoCOMPRAS.TVentas Then
            sStoredProcedure = "SEI_DescuentosLin"
        Else
            sStoredProcedure = "SEI_DescuentosLinC"
        End If
        '
        Dim oRS As SAPbobsCOM.Recordset = Nothing
        oRS = oCompany.GetBusinessObject(BoRecordset)
        oRS.DoQuery("EXEC " & sStoredProcedure & " " & _
                                               sC(sCategoria) & "," & _
                                               sZona & "," & _
                                               sC(sActividad) & "," & _
                                               sC(sInterlocutor) & "," & _
                                               iMarca.ToString & "," & _
                                               iFamilia.ToString & "," & _
                                               sC(sSubFamilia) & "," & _
                                               sC(sArticulo) & "," & _
                                               dCantidad.ToString.Replace(",", ".") & "," & _
                                               sC(sFecha) & "," & _
                                               sC(String.Format(TIPOVENTASoCOMPRAS.TVentas)))

        '
        If Not oRS.EoF Then
            lDescuento_Linea = oRS.Fields.Item(0).Value
        End If


        ' lDescuento_Linea = oRS.Fields.Item(0).Value
        '
        LiberarObjCOM(oRS)

        '
        DESCUENTO_LINIA = lDescuento_Linea
        '
        LiberarObjCOM(oRecordsetItem)
        LiberarObjCOM(oRecordsetInterlocutor)
        '
    End Function

#Region "Funciones Descuento"
    Public Function Calculo_Descuento_Total(ByVal dDescuento1 As Double, _
                                            ByVal dDescuento2 As Double, _
                                            ByVal dDescuento3 As Double, _
                                            ByVal dDescuento4 As Double, _
                                            ByVal dDescuento5 As Double) As Double
        '
        'A partir de 5 descomptes retorna el seu únic descompte equivalent
        '
        Dim lDescuento1 As Double
        Dim lDescuento2 As Double
        Dim lDescuento3 As Double
        Dim lDescuento4 As Double
        Dim lDescuento5 As Double
        Dim lDescuento As Double
        '
        'Calculo_Descuento_Total = dDescuentoActual
        '
        'If dDescuento1 = 0 And dDescuento2 = 0 And dDescuento3 = 0 And dDescuento4 = 0 And dDescuento5 = 0 Then Exit Function
        '
        lDescuento1 = 1 - (dDescuento1 / 100)
        lDescuento2 = 1 - (dDescuento2 / 100)
        lDescuento3 = 1 - (dDescuento3 / 100)
        lDescuento4 = 1 - (dDescuento4 / 100)
        lDescuento5 = 1 - (dDescuento5 / 100)
        lDescuento = (1 - (lDescuento1 * lDescuento2 * lDescuento3 * lDescuento4 * lDescuento5)) * 100
        '
        Calculo_Descuento_Total = lDescuento
        '
    End Function
    '
    Private Function ObtenerDatosIC(ByVal sSQL As String, _
                                    ByRef sCategoria As String, _
                                    ByRef sZona As String, _
                                    ByRef sActividad As String,
                                    ByRef oCompany As SAPbobsCOM.Company) As Boolean
        '
        Dim oRcs As SAPbobsCOM.Recordset
        '
        ObtenerDatosIC = False
        '
        oRcs = oCompany.GetBusinessObject(BoRecordset)
        oRcs.DoQuery(sSQL)
        '
        If Not oRcs.EoF Then
            '
            ObtenerDatosIC = True
            '
            sCategoria = oRcs.Fields.Item("U_SEICateg").Value
            sZona = oRcs.Fields.Item("SlpCode").Value
            sActividad = oRcs.Fields.Item("U_SEIActi").Value
        End If
        '
        LiberarObjCOM(oRcs)
        '
    End Function
    '
    Private Function ObtenerDatos_ITEM(ByVal sSQL As String, _
                                       ByRef lMarca As Long, _
                                       ByRef lFamilia As Long, _
                                       ByRef sSubFamilia As String,
                                       ByRef oCompany As SAPbobsCOM.Company) As Boolean
        '
        Dim oRcs As SAPbobsCOM.Recordset
        '
        ObtenerDatos_ITEM = False
        '
        oRcs = oCompany.GetBusinessObject(BoRecordset)
        oRcs.DoQuery(sSQL)
        '
        If Not oRcs.EoF Then
            '
            ObtenerDatos_ITEM = True
            '
            lMarca = NullToLong(oRcs.Fields.Item("FirmCode").Value)
            lFamilia = NullToLong(oRcs.Fields.Item("ItmsGrpCod").Value)
            sSubFamilia = oRcs.Fields.Item("U_SEISubFa").Value
        End If
        '
        LiberarObjCOM(oRcs)
    End Function
    '
    Private Function ObtenerLinea(ByVal sSQL As String, ByRef lDescuento_Linea As Long, ByRef oCompany As SAPbobsCOM.Company) As Boolean
        '
        Dim oRcs As SAPbobsCOM.Recordset
        '
        ObtenerLinea = False
        '
        oRcs = oCompany.GetBusinessObject(BoRecordset)
        oRcs.DoQuery(sSQL)
        '
        If Not oRcs.EoF Then
            If NullToLong(oRcs.Fields.Item("DocEntry").Value) <> 0 Then
                ObtenerLinea = True
                lDescuento_Linea = NullToLong(oRcs.Fields.Item("DocEntry").Value)
            End If
        End If
        '
        LiberarObjCOM(oRcs)
        '
    End Function
    '
    Private Function ObtenerPromocion(ByVal sCategoria As String, _
                                        ByVal sZona As String, _
                                        ByVal sActividad As String, _
                                        ByVal sInterlocutor As String, _
                                        ByVal iMarca As Long, _
                                        ByVal iFamilia As Long, _
                                        ByVal sSubFamilia As String, _
                                        ByVal sArticulo As String, _
                                        ByVal sFecha As String,
                                        ByRef oCompany As SAPbobsCOM.Company) As String
        '
        Dim ls As String
        Dim sWhere As String
        '
        Dim oRcs As SAPbobsCOM.Recordset
        '
        Dim lsAnd As String
        '
        ObtenerPromocion = "NO"
        '
        sWhere = ""
        lsAnd = " AND U_SEIpromo='SI' "
        '
        ls = ""
        ls = ls & "SELECT TOP 1 Code, U_SEIpromo,U_SEIActi,U_SEIemple,U_SEICateg,U_SEIMarca,"
        ls = ls & " U_SEIFam,U_SEISubFa,U_SEIArt,U_SEIIni,U_SEIfin "
        ls = ls & " FROM [@SEIDESCUENTOSCLI] "
        '
        If sInterlocutor <> "" Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIInt,'') = '" & sInterlocutor & "'" & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIInt,'') = '" & sInterlocutor & "'" & lsAnd & ")"
            End If
        End If
        '
        'Actividad
        If sActividad <> "" Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIActi,'') = '" & sActividad & "'" & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIActi,'') = '" & sActividad & "'" & lsAnd & ")"
            End If
        End If
        '
        ' Antes Zona - Ahora Empleado de Ventas
        If NullToLong(sZona) <> 0 Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIemple,0) = " & NullToLong(sZona).ToString & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIemple,0) = " & NullToLong(sZona).ToString & lsAnd & ")"
            End If
        End If
        '
        'Categoria
        If sCategoria <> "" Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEICateg,'') = '" & sCategoria & "'" & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEICateg,'') = '" & sCategoria & "'" & lsAnd & ")"
            End If
        End If
        '
        'Marca
        If NullToLong(iMarca) <> 0 Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIMarca,0) = " & iMarca.ToString & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIMarca,0) = " & iMarca.ToString & lsAnd & ")"
            End If
        End If
        '
        'Familia
        If NullToLong(iFamilia) <> 0 Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIFam,0) = " & iFamilia.ToString & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIFam,0) = " & iFamilia.ToString & lsAnd & ")"
            End If
        End If
        '
        'SubFamilia
        If sSubFamilia <> "" Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEISubFa,'') = '" & sSubFamilia & "'" & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEISubFa,'') = '" & sSubFamilia & "'" & lsAnd & ")"
            End If
        End If
        '
        'Artículo
        If sArticulo <> "" Then
            If sWhere = "" Then
                sWhere = sWhere & " WHERE (ISNULL(U_SEIArt,'') = '" & sArticulo & "'" & lsAnd & ")"
            Else
                sWhere = sWhere & " OR (ISNULL(U_SEIArt,'') = '" & sArticulo & "'" & lsAnd & ")"
            End If
        End If
        '
        'Fecha
        sWhere = sWhere & " AND ('" & sFecha & "' BETWEEN U_SEIIni AND U_SEIFin) "
        sWhere = sWhere & " AND (U_SEIIni IS NOT NULL " & lsAnd & ")"
        sWhere = sWhere & " AND (U_SEIFin IS NOT NULL " & lsAnd & ")"
        '
        ls = ls & sWhere
        '
        oRcs = oCompany.GetBusinessObject(BoRecordset)
        oRcs.DoQuery(ls)
        '
        If Not oRcs.EoF Then
            '
            If oRcs.Fields.Item("Code").Value.ToString.Trim <> "" Then
                ObtenerPromocion = "SI"
            End If
        End If
        '
        LiberarObjCOM(oRcs)
        '
    End Function
    '
#End Region


#Region "Funciones Tipos de Datos"

    Function Formato_Decimales_IG(ByVal Valor As Object) As String

        Valor = Valor.ToString.Replace(".", "")
        Valor = Valor.ToString.Replace(",", ".")
        Return Valor.ToString

    End Function

    Function NullToText(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.ToString.Trim = "" Then
            Return " "
        Else
            Return Valor.ToString
        End If

    End Function

    Function NullToInt(ByVal Valor As Object) As Integer

        If IsDBNull(Valor) Or Valor.ToString.Trim = "" Then
            Return 0
        Else
            Return Convert.ToInt32(Valor.ToString)   ' Pasar a integer
        End If

    End Function

    Function NullToDoble(ByRef Valor As Object) As Double

        If IsDBNull(Valor) Or Trim(Valor.ToString) = "" Then
            Return 0
        Else
            Return Convert.ToDouble(Valor)  ' Pasar a double
        End If

    End Function

    Function NullToData(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "NULL"
        Else
            Return String.Format("{0:d}", Valor)
        End If

    End Function

    Function NullToHora(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "NULL"
        Else
            Return String.Format("{0:t}", Valor)
        End If

    End Function

    Function NullToLong(ByVal Valor As Object) As Long

        If IsDBNull(Valor) Or Trim(Valor.ToString) = "" Then
            Return 0
        Else
            Return CType(Valor, Long)   ' Pasar a Long
        End If

    End Function

    Function NullToSiNo(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Function IntToBooleanS_N(ByVal Valor As Object) As String

        If IsDBNull(Valor) Then
            Return "N"
        ElseIf Valor = 0 Then
            Return "N"
        Else
            Return "S"
        End If

    End Function

    Function IntToBoolean(ByVal Valor As Object) As String

        If IsDBNull(Valor) Then
            Return "N"
        ElseIf Valor = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Function BooleanToInt(ByVal Valor As Object) As Integer

        If Valor = "True" Then
            Return "1"
        Else
            Return "0"
        End If

    End Function
    '
    Public Function NowDateToString() As String
        NowDateToString = Now.Date.ToString("yyyyMMdd")
    End Function
    '
    ' Poner un valor entre comillas
    Public Function sC(ByVal sValor As String) As String
        sC = "'" & sValor.Replace("'", "''") & "'"
    End Function

#End Region

    '
    Public Sub LiberarObjCOM(ByRef oObjCOM As Object, Optional ByVal bCollect As Boolean = False)
        '
        'Liberar y destruir Objecto com 
        ' En los UDO'S es necesario utilizar GC.Collect  para eliminarlos de la memoria
        If Not IsNothing(oObjCOM) Then
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oObjCOM)
            oObjCOM = Nothing
            If bCollect Then
                GC.Collect()
            End If
        End If

    End Sub


End Class
