﻿'Option Explicit On
''
'Imports System.Data.SqlClient
'Imports System.Text
'Imports System.IO
'Imports SAPbobsCOM.BoObjectTypes

'Public Class clsMessageSAP
'    '
'    '    Public Structure st_Usuario
'    '        Public UserCode As String
'    '        Public EmailAddress As String
'    '    End Structure

'    '    Private stUsuario As st_Usuario

'    '    Private EmpleadoVentas As Long
'    '    Private sMsgError As String
'    '    Private sUserCode As String
'    '    Private sEmail As String
'    '    Private dtOrder As DataTable = Nothing
'    '    '
'    '    Private PedidoPDA As String

'    '#Region "Contructor"
'    '    '
'    '    Public Sub New()
'    '        'if creating controls via code, use initialize
'    '        Initialize()
'    '    End Sub

'    '    Private Sub Initialize()

'    '    End Sub
'    '    '
'    '    '
'    '#End Region

'    '    Public Sub EnviarMensajes(ByVal oCompany As SAPbobsCOM.Company)
'    '        '
'    '        Dim ls As String
'    '        Dim oRcs As SAPbobsCOM.Recordset = Nothing
'    '        Dim sSubject As String = ""
'    '        Dim aUsuarios As ArrayList = Nothing
'    '        Try
'    '            '
'    '            ' Usuarios Sap (Delegaciones para enviar mensajes)
'    '            '
'    '            ls = ""
'    '            ls = ls & " Select T0.[Code]"
'    '            ls = ls & " ,T0.[Name]"
'    '            ls = ls & " ,T0.[U_SEIslpco]"
'    '            ls = ls & " ,T0.[U_SEIussap]"
'    '            ls = ls & " ,T1.[SlpName]"
'    '            ls = ls & " FROM [@SEIOUSRE] T0 LEFT OUTER JOIN OSLP T1 ON T0.U_SEIslpco=T1.SlpCode"
'    '            ls = ls & " Where U_SEIslpco =" & EmpleadoVentas.ToString
'    '            '
'    '            oRcs = oCompany.GetBusinessObject(BoRecordset)
'    '            oRcs.DoQuery(ls)
'    '            '
'    '            If oRcs.RecordCount <> 0 Then
'    '                '
'    '                aUsuarios = New ArrayList
'    '                '
'    '                sSubject = "-PDA - Incidencia Crear Pedido de Ventas"
'    '                sMsgError = "Incidencia al crer Pedido de Ventas PDA: " & Me.PedidoPDA.ToString & vbCrLf & vbCrLf & _
'    '                            " Cliente: " & dtOrder.Rows(0).Item("Cliente").ToString & vbCrLf & _
'    '                            " Agente: " & dtOrder.Rows(0).Item("Agente").ToString & _
'    '                            " " & oRcs.Fields.Item("SlpName").Value & vbCrLf & _
'    '                            " Ref.Pedido: " & dtOrder.Rows(0).Item("NumAtCard") & vbCrLf & _
'    '                sMsgError
'    '                '
'    '                Do Until oRcs.EoF
'    '                    '
'    '                    '
'    '                    ObtenerDatosUsuario(oRcs.Fields.Item("U_SEIussap").Value, sUserCode, sEmail, oCompany)
'    '                    '
'    '                    Me.stUsuario = New st_Usuario
'    '                    Me.stUsuario.UserCode = sUserCode
'    '                    Me.stUsuario.EmailAddress = sEmail

'    '                    aUsuarios.Add(Me.stUsuario)
'    '                    '
'    '                    oRcs.MoveNext()
'    '                Loop
'    '                '
'    '                CrearMensajeSAP(oCompany, sSubject, sMsgError, aUsuarios)
'    '                ' 
'    '            End If
'    '            '
'    '        Catch ex As Exception
'    '            Throw ex
'    '        Finally
'    '            LiberarObjCOM(oRcs)
'    '        End Try


'    '        '
'    '    End Sub

'    '    Public Sub ObtenerDatosUsuario(ByVal sUsuarioSap As String, _
'    '                                    ByRef sUserCode As String, _
'    '                                    ByRef sEmail As String, ByVal oCompany As SAPbobsCOM.Company)
'    '        '
'    '        Dim ls As String
'    '        Dim oRcs As SAPbobsCOM.Recordset = Nothing
'    '        Try
'    '            ls = ""
'    '            ls = ls & " SELECT E_MAIL, USER_CODE, U_NAME, USERID"
'    '            ls = ls & " FROM OUSR"
'    '            ls = ls & " Where USERID =" & sUsuarioSap
'    '            '
'    '            oRcs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'    '            oRcs.DoQuery(ls)
'    '            '
'    '            If Not oRcs.EoF Then
'    '                '
'    '                sUserCode = oRcs.Fields.Item("USER_CODE").Value
'    '                sEmail = oRcs.Fields.Item("E_MAIL").Value
'    '                '
'    '            End If
'    '        Catch ex As Exception
'    '            Throw ex
'    '        Finally
'    '            LiberarObjCOM(oRcs)
'    '        End Try

'    '    End Sub

'    '    Private Sub LiberarObjCOM(ByRef oObjCOM As Object, Optional ByVal bCollect As Boolean = False)

'    '        'Liberar y destruir Objeto com 
'    '        If Not IsNothing(oObjCOM) Then
'    '            System.Runtime.InteropServices.Marshal.ReleaseComObject(oObjCOM)
'    '            oObjCOM = Nothing
'    '            If bCollect Then
'    '                GC.Collect()
'    '            End If
'    '        End If
'    '    End Sub

'    '    Public Sub CrearMensajeSAP(ByVal oCompany As SAPbobsCOM.Company, ByVal sSubject As String, _
'    '                               ByVal sMessageText As String, _
'    '                               ByRef aUsuarios As ArrayList, _
'    '                               Optional ByVal sFichero As String = "")
'    '        '
'    '        Dim oMsg As SAPbobsCOM.Messages = Nothing
'    '        Dim sError As String
'    '        Dim stUsuario As st_Usuario
'    '        Dim iLine As Integer
'    '        '
'    '        Try
'    '            iLine = -1
'    '            oMsg = oCompany.GetBusinessObject(oMessages)
'    '            '
'    '            oMsg.MessageText = sMessageText                '"Se ha realizado la Entrada de Mercancías Nº " & "1"
'    '            oMsg.Subject = Mid(sSubject, 1, 50)                      '"Mensaje EDI"
'    '            '
'    '            For Each stUsuario In aUsuarios
'    '                If stUsuario.UserCode <> "" AndAlso stUsuario.EmailAddress <> "" Then
'    '                    iLine = iLine + 1
'    '                    'there are one recipients in this message
'    '                    oMsg.Recipients.Add()
'    '                    'set values for the first recipients
'    '                    oMsg.Recipients.SetCurrentLine(iLine)
'    '                    oMsg.Recipients.UserCode = stUsuario.UserCode
'    '                    oMsg.Recipients.NameTo = stUsuario.UserCode
'    '                    oMsg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tYES
'    '                    oMsg.Recipients.EmailAddress = stUsuario.EmailAddress
'    '                End If
'    '                '
'    '            Next

'    '            'set referenced doc
'    '            'Call oMsg.AddDataColumn("Por favor revisar",)
'    '            'add attachment

'    '            If File.Exists(sFichero) Then
'    '                Call oMsg.Attachments.Add()
'    '                oMsg.Attachments.Item(0).FileName = sFichero
'    '            End If
'    '            '
'    '            'send it out
'    '            If oMsg.Add <> 0 Then
'    '                'Obtener el código de error y mensaje de error
'    '                sError = RecuperarErrorSap(oCompany)
'    '                Throw New Exception(sError)
'    '                ' TODO: Grabar error log de modificaciones
'    '            End If
'    '            '
'    '        Catch ex As Exception
'    '            Throw New Exception(ex.Message)
'    '        Finally
'    '            LiberarObjCOM(oMsg)
'    '        End Try
'    '        '
'    '        '
'    '    End Sub

'    '    Public Function RecuperarErrorSap(ByVal oCompany As SAPbobsCOM.Company) As String

'    '        Dim sError As String
'    '        Dim lErrCode As Long
'    '        Dim sErrMsg As String
'    '        '
'    '        lErrCode = 0
'    '        sErrMsg = ""
'    '        oCompany.GetLastError(lErrCode, sErrMsg)
'    '        sError = "Error: " & lErrCode.ToString & " " & sErrMsg
'    '        '
'    '        Return sError
'    '        '
'    '    End Function

'End Class
