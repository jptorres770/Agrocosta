﻿Public Class EntFacturaPendienteCab

    Public Property DocEntry As Integer
    Public Property NumeroFactura As Integer
    Public Property FechaFactura As Date
    Public Property FechaVencimiento As Date
    Public Property ImporteTotal As Double
    Public Property Moneda As String
    Public Property ImportePagado As Double
    Public Property IDCliente As String
    Public Property ImportePendiente As Double
    Public Property ImporteAPagar As Double
    Public Property NumeroFacturaVendedor As String

End Class
