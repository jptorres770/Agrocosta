﻿Option Strict On
Public Class EntPedidoCab

    Public Property NumCab As Long

    Public Property CardCode As String
    Public Property CardName As String
    Public Property DocDate As Integer
    Public Property EmpID As Integer
    Public Property FechaEntrega As Integer
    Public Property Longitud As String
    Public Property Latitud As String
    Public Property Comentarios As String
    Public Property HoraCreacion As Integer
    Public Property CondicionPago As String
    Public Property TotalBruto As Double
    Public Property TotalNeto As Double
    Public Property DtoGralPorcent As Double
    Public Property DtoGralEuros As Double
    Public Property TotalIVAIncluido As Double
    Public Property RecargoEquivalencia As String
    Public Property IDDireccion As String
    Public Property Direccion As String
    Public Property Poblacion As String
    Public Property CP As String
    Public Property Ciudad As String
    Public Property Provincia As String
    Public Property NumeroPedidoCliente As String
    Public Property RutaID As String
    Public Property RutaSeleccionadaUsuario As String


    Public Property EnvioMail As String

    Public Property Sincro As String
    Public Property FechaSincro As Integer
    Public Property IncidenciaSincro As String

End Class
