﻿Option Strict On
Public Class EntFacturaPendienteLin

    Public Property DocEntry As Long
    Public Property CodigoArticulo As String
    Public Property DescripcionArticulo As String
    Public Property Cantidad As Double
    Public Property Price As Double
    Public Property Moneda As String
    Public Property Almacen As String

End Class
