
ALTER VIEW [dbo].[TAB_CLIENTECOMPARATIVA]
AS
SELECT
Vista.CardCode,
Vista.CardName,
Vista.EmpID,
Vista.ImpActual,
Vista.ImpAnterior,
Vista.ImpActual - Vista.ImpAnterior  AS Diferencia,
100 - ((Vista.ImpAnterior  * 100 / CASE WHEN Vista.ImpActual= 0 THEN 1 ELSE Vista.ImpActual END)) AS PorcDiferencia
FROM (
SELECT
T0.Codigo AS CardCode,
T0.Nombre AS CardName,
ISNULL(T0.EmpID, 0) AS EmpID,
--ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = T0.Codigo AND YEAR(P.DocDate) = Year(GetDate())-1) ,0) as ImpAnterior, 
--ISNULL((SELECT SUM(P.DocTotal) FROM OINV P WHERE P.CardCode = T0.Codigo AND YEAR(P.DocDate) = Year(GetDate()) ),0) as ImpActual 
ISNULL((SELECT SUM(L.LineTotal) FROM OINV P INNER JOIN INV1 L ON P.DocEntry = L.DocEntry WHERE P.CardCode = T0.Codigo AND P.SlpCode = T1.SlpCode AND P.DocDate BETWEEN DATEADD(YEAR, -2, GETDATE()) AND DATEADD(YEAR, -1, GETDATE())) ,0) as ImpAnterior, 
ISNULL((SELECT SUM(L.LineTotal) FROM OINV P INNER JOIN INV1 L ON P.DocEntry = L.DocEntry WHERE P.CardCode = T0.Codigo AND P.SlpCode = T1.SlpCode AND P.DocDate BETWEEN DATEADD(YEAR, -1, GETDATE()) AND GetDate()) ,0) as ImpActual 
FROM TAB_CLIENTES T0
INNER JOIN TAB_USUARIOS T1 ON T0.EmpID = T1.EmpID
) AS Vista

GO


