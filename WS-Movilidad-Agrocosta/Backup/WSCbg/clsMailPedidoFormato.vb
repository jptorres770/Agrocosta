﻿Option Strict On
Imports Entidades

Public Class clsMailPedidoFormato

    Private mPedido As SAPbobsCOM.Documents
    Private mDocNum As String

    Sub New(ByRef PedidoCab As SAPbobsCOM.Documents, ByVal DocNum As String)

        mPedido = PedidoCab
        mDocNum = DocNum

    End Sub

    Public Function getHTML() As String

        Dim SHTML As String = ""
        Try
            SHTML &= "<p>Pedido realizado a " & Me.mPedido.CardCode & " - " & Me.mPedido.CardName & " con número <b>" & mDocNum & "</b> se ha procesado correctamente.</p>"

            If mPedido.NumAtCard <> "" Then
                SHTML &= "<p>Referencia cliente: " & Me.mPedido.NumAtCard & "</p>"
            End If


            SHTML &= "<p>" & FormatearLineasTabla() & "</p>"
            SHTML &= "<p>&nbsp;</p>"


            SHTML &= "<p><x-small>Este correo se ha generado de forma automática y no está gestionado. Por favor no responda a esta dirección.</x-small></p>"
            SHTML &= "<p><x-small>La información incluida en el presente correo electrónico es CONFIDENCIAL, siendo para uso exclusivo del destinatario arriba mencionado. Si usted recibe este mensaje y no es el destinatario, el empleado, el agente responsable, o no desea recibir este tipo de comunicación, por favor póngase en contacto con la dirección de correo indicada en el pie del presente e-mail.</p></x-small>"
            SHTML &= "<p><x-small>Antes de imprimir este e-mail piense bien si es necesario hacerlo: El medioambiente es cosa de todos.</p></x-small>"

            SHTML &= "<p><small><b>www.cbg.es - cbg@cbg.es</b></small></p>"

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return SHTML

    End Function

    Private Function FormatearLineasTabla() As String
        Dim SHTML As String = ""
        Try


            'Tabla
            SHTML &= "<p>Detalle del pedido:</p>"
            SHTML &= "<table border='1'>"
            SHTML &= "<tr>"
            SHTML &= "<th colspan='10' align='center' bgcolor='black'><font color='white'><b>Desglose del pedido</b></font></th>"
            SHTML &= "</tr>"
            SHTML &= "<tr>"
            SHTML &= "<th>Código</th>"
            SHTML &= "<th>Descripción</th>"
            SHTML &= "<th>Uds./caja</th>"
            SHTML &= "<th>Cajas</th>"
            SHTML &= "<th>Kgs.</th>"
            SHTML &= "<th>Cantidad</th>"
            SHTML &= "<th>Oferta</th>"
            SHTML &= "<th>Dto.</th>"
            SHTML &= "<th>Precio</th>"
            SHTML &= "<th>Total</th>"
            SHTML &= "</tr>"

            Dim SumLineas As Double
            For iLinea As Integer = 0 To Me.mPedido.Lines.Count - 1
                mPedido.Lines.SetCurrentLine(iLinea)

                With mPedido.Lines
                    SHTML &= "<tr>"
                    SHTML &= "<td>" & .ItemCode & "</td>"
                    SHTML &= "<td>" & .ItemDescription & "</td>"
                    SHTML &= "<td>" & .Factor3 & "</td>"
                    SHTML &= "<td>" & .Factor1 & "</td>"
                    SHTML &= "<td>" & .Factor2 & "</td>"
                    SHTML &= "<td align='right'><b>" & .Quantity & "</b></td>"

                    If .UserFields.Fields.Item("U_SEIofertacad").Value.ToString <> "N" Then
                        SHTML &= "<td align='center'>X</td>"
                    Else
                        SHTML &= "<td>&nbsp;</td>"
                    End If

                    SHTML &= "<td>" & .DiscountPercent.ToString("N") & "%</td>"
                    SHTML &= "<td>" & .Price.ToString("C") & "</td>"
                    SHTML &= "<td>" & .LineTotal.ToString("C") & "</td>"
                    SHTML &= "</tr>"

                    SumLineas += .LineTotal
                End With

            
            Next

            SHTML &= "<tr><td colspan='10'>&nbsp;</td></tr>"

            'SUBTOTAL
            SHTML &= "<tr>"
            SHTML &= "<td colspan='9' align='right'>Subtotal:</td>"
            SHTML &= "<td>" & SumLineas.ToString("C") & "</td>"
            SHTML &= "</tr>"

            'DESCUENTOS
            If Me.mPedido.DiscountPercent <> 0 Then
                SHTML &= "<tr>"
                SHTML &= "<td colspan='9' align='right'>Dto. sobre total:</td>"
                SHTML &= "<td>" & Me.mPedido.DiscountPercent.ToString("N") & "%</td>"
                SHTML &= "</tr>"
            End If

            'TOTAL
            SHTML &= "<tr>"
            SHTML &= "<td colspan='9' align='right'><b>Total (IVA Incl.):</b></td>"
            SHTML &= "<td><b>" & Me.mPedido.DocTotal.ToString("C") & "</b></td>"
            SHTML &= "</tr>"

            SHTML &= "</table>"
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return SHTML

    End Function


End Class
