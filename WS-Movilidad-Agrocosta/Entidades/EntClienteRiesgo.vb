﻿Public Class EntClienteRiesgo

    Public Property CardCode As String
    Public Property CardName As String

    Public Property EmpID As Integer
    Public Property TipoDoc As String
    Public Property NumDoc As String
    Public Property Comentarios As String
    Public Property EV As Integer
    Public Property FechaContable As Integer
    Public Property FechaVencimiento As Integer
    Public Property Riesgo As Double
    Public Property CL As String
    Public Property FormaCobro As String
    Public Property ViaCobro As String
    Public Property TotalDoc As Double
    Public Property Pendiente As Double
    Public Property TotalEfecto As Double
    Public Property FechaVencimientoEfecto As Integer
    Public Property NumEfecto As Integer
    Public Property Banco As String
    Public Property Estado As String
    Public Property NumRemesa As Integer


End Class
