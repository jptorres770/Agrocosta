﻿Public Class EntClienteDireccion

    'Nuevo 04/2016
    Public Property RutaNombre As String
    Public Property RutaID As String

    Public Property Lunes As String
    Public Property Martes As String
    Public Property Miercoles As String
    Public Property Jueves As String
    Public Property Viernes As String
    Public Property Sabado As String

    Public Property EmpID As Integer

    'Anterior
    Public Property CardCode As String
    Public Property IDDireccion As String
    Public Property Direccion As String
    Public Property Poblacion As String
    Public Property CP As String
    Public Property Ciudad As String
    Public Property Provincia As String

    Public Property Latitud As String
    Public Property Longitud As String

    Public Property Predeterminada As String

    Public Property Sincro As String
    Public Property FechaSincro As Integer
    Public Property IncidenciaSincro As String

End Class
