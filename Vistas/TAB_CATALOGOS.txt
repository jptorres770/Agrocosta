
ALTER VIEW [dbo].[TAB_CATALOGOS]
AS
select 
	 ISNULL([U_SEIIDCatalogo],0) as IDCatalogo
      ,ISNULL([U_SEINombre],'') as NombreCatalogo
      ,ISNULL([U_SEICardCode] ,'') as CardCode
      ,ISNULL([U_SEICardName],'') as CardName
      ,CONVERT(char(10), ISNULL([U_SEIFechaCrea],'20000101'), 112) as FechaCreacion
      ,ISNULL([U_SEINotas],'') as Notas
      ,ISNULL([U_SEIEmpID],0) as EmpID
      ,'S' as Sincro
      ,'' as ErrorSincro
from [@SEICATALOGO] T0

GO


