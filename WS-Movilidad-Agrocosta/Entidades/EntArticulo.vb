﻿Public Class EntArticulo

    Public Property ItemCode As String
    Public Property ItemDesc As String
    Public Property RutaFoto As String
    Public Property RutaFicha As String
    Public Property ListaPrecioCompra As String
    Public Property ListaPrecioVenta As String

    Public Property EAN As String
    Public Property FamiliaCode As Integer
    Public Property FamiliaName As String

    Public Property SubFamiliaCode As String
    Public Property SubFamiliaName As String

    Public Property FabricanteCode As Integer
    Public Property FabricanteName As String

    Public Property ProveedorCode As String
    Public Property ProveedorName As String

    Public Property UnidadVenta As String

    Public Property UnidadesPorCaja As Double
    Public Property Cajas As Double
    Public Property Kilos As Double

    Public Property IVA As Double
    Public Property IVARecargo As Double

    'Public Property interno_seleccionado As Boolean

    Public Property Paletizacion1 As Double
    Public Property Paletizacion2 As Double
    Public Property Paletizacion3 As Double

    Public Property PlazoServicio As String
    Public Property SobrePedido As String


    Public Property Notas As String

    Public Property Novedad As String
    Public Property Promocion As String

End Class
