﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Newtonsoft.Json

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://seidor.com/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WSJSON
    Inherits System.Web.Services.WebService

    'traer pedidos portal
    <WebMethod()>
    Public Function getPedidosPORTAL() As String

        Try

            'If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
            'Return "Empleado incorrecto"
            'End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getPedidosPORTAL())

            If strJSON <> "" Then
                'Return clsUtil.CompimirGZipBase64(strJSON)
                Return strJSON
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try

    End Function

    <WebMethod()>
    Public Function LoginPortalWEB(ByVal User As String, ByVal Password As String) As String

        Try

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.LoginPortalWEB(User, Password))

            If strJSON <> "" Then
                'Return clsUtil.CompimirGZipBase64(strJSON)
                Return strJSON
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try

    End Function


    <WebMethod()> _
    Public Function getDIAPIOnline(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try


            If New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) = False Then
                clsLog.Log.Fatal("Empleado incorrecto" & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return "5002"
            End If

            If checkLoginSAP(Empleado) = False Then
                clsLog.Log.Fatal("Empleado No Login en SAP" & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return "5003"
            End If

            Return "0"
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return "5001"
        Finally

        End Try
    End Function

    <WebMethod()> _
    Public Sub isOnline()

        Try
            Return
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally

        End Try
    End Sub

    <WebMethod()> _
    Public Function getUltimaVersion() As String

        Dim retVal As String = ""
        Try
            retVal = ConfigurationManager.AppSettings.Get("lastVersion").ToString

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally

        End Try

        Return retVal

    End Function

    <WebMethod()> _
    Public Function TestMail(ByVal DestinatariosSeparadosPorComas As String) As String

        Try
            Dim mail As New clsMail
            mail.pASUNTO = "TestMail"
            mail.pCUERPO = "Cuerpo"

            mail.SendMail(DestinatariosSeparadosPorComas)


        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        Finally

        End Try

        Return "Pruebas finalizadas"

    End Function

    'GETTERS ARTICULOS

    <WebMethod()> _
    Public Function getArticulos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulos(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If


        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    '<WebMethod()>
    'Public Function getArticulosOfertas(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulosOfertas(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If


    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    <WebMethod()> _
    Public Function getArticulosPrecios(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulosPrecios(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If


        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    '<WebMethod()> _
    'Public Function getArticulosPremios(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulosPremios(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If


    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    <WebMethod()> _
    Public Function getArticulosStocks(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulosStock(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If


        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function


    'GETTER DESCUENTOS

    '<WebMethod()>
    'Public Function getDescuentos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getDescuentos(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function


    'GETTER BANCO

    '<WebMethod()>
    'Public Function getBancos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getBancos(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If


    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function


    'GETTERS CLIENTES

    <WebMethod()> _
    Public Function getClientes(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientes(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    '<WebMethod()> _
    'Public Function getClientesComparativa(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesComparativa(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    '<WebMethod()> _
    'Public Function getClientesComparativaDetalle(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesComparativaDetalle(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    <WebMethod()> _
    Public Function getClientesContactos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesContactos(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    <WebMethod()> _
    Public Function getClientesDirecciones(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesDirecciones(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    <WebMethod()> _
    Public Function getClientesRutas(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesRuta(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    '<WebMethod()> _
    'Public Function getClientesRiesgo(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getClientesRiesgo(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    'GETTER DOCUMENTOS

    '<WebMethod()> _
    'Public Function getDocumentosHistoricoCab(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getHistoricoDocumentosCab(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    '<WebMethod()> _
    'Public Function getDocumentosHistoricoLin(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getHistoricoDocumentosLin(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function


    'GETTER COBROS
    '<WebMethod()>
    'Public Function getFacturasCobro(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getFacturasCobro(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    <WebMethod()> _
    Public Function getDocumentosPendientesCab(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getDocumentosPendientesCab(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    <WebMethod()>
    Public Function getDocumentosPendientesLin(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getDocumentosPendientesLin(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function


    'GETTERS MENSAJES

    '<WebMethod()> _
    'Public Function getMensajesEntrada(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getMensajesEntrada(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try

    'End Function



    'GETTERS USUARIOS

    <WebMethod()> _
    Public Function getUsuarios(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try
            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getUsuarios(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try

    End Function

    '<WebMethod()> _
    'Public Function getNotas(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getNotas(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    '<WebMethod()> _
    'Public Function getArticulosHistoricosDesactivados(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getArticulosHistoricosDesactivados(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    'GETTERS CATALOGOS

    <WebMethod()> _
    Public Function getCatalogos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getCatalogos(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function

    <WebMethod()> _
    Public Function getCatalogosArticulos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oBaja As New clsDatosBajada
            Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getCatalogoArticulos(Empleado, Version))

            If strJSON <> "" Then
                Return clsUtil.CompimirGZipBase64(strJSON)
            Else
                Return ""
            End If

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try


    End Function



    'GETTER FAVORITOS

    '<WebMethod()> _
    'Public Function getFavoritos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getFavoritos(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    'GETTERS TESORERIA

    '<WebMethod()> _
    'Public Function getIngresosCab(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getIngresosCab(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function

    '<WebMethod()> _
    'Public Function getIngresosLin(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer) As String

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oBaja As New clsDatosBajada
    '        Dim strJSON As String = JsonConvert.SerializeObject(oBaja.getIngresosLin(Empleado, Version))

    '        If strJSON <> "" Then
    '            Return clsUtil.CompimirGZipBase64(strJSON)
    '        Else
    '            Return ""
    '        End If

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try


    'End Function


    'SETTERS

    <WebMethod()>
    Public Function setPedido(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJsonCab As String, strJsonLin As String) As String

        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If


            If Not New clsDatosSubida(Me).checkLoginSAP() Then
                Return "No hay login SAP"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            clsLog.Log.Info("oSubida:" + oSubida.ToString)
            Dim strJSON As String = JsonConvert.SerializeObject(oSubida.setPedidos(Empleado, strJsonCab, strJsonLin))
            Return strJSON
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try

    End Function

    <WebMethod()>
    Public Function setActividad(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String
        Try

            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            clsLog.Log.Info("oSubida:" + oSubida.ToString)
            Dim strJSON2 As String = JsonConvert.SerializeObject(oSubida.setActividad(Empleado, strJson))
            Return strJSON2

            If Not New clsDatosSubida(Me).checkLoginSAP() Then
                Return "No hay login SAP"
            End If
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return ex.Message
        End Try
    End Function


    <WebMethod()> _
    Public Function setCatalogos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJsonCab As String, strJsonLin As String) As String

        Dim retVal As String = ""

        Try


            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            retVal = oSubida.setCatalogos(Empleado, strJsonCab, strJsonLin)

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
        End Try

        Return retVal

    End Function

    '<WebMethod()> _
    'Public Function setFavoritos(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

    '    Dim retVal As String = ""

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setFavoritos(Empleado, strJson)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        retVal = ex.Message
    '    End Try

    '    Return retVal

    'End Function

    '<WebMethod()> _
    'Public Function setNotas(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

    '    Dim retVal As String = ""

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setNotas(Empleado, strJson)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        retVal = ex.Message
    '    End Try

    '    Return retVal

    'End Function


    '<WebMethod()> _
    'Public Function setArticulosHistoricosDesactivados(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

    '    Dim retVal As String = ""

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setArticulosHistoricosDesactivados(Empleado, strJson)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        retVal = ex.Message
    '    End Try

    '    Return retVal

    'End Function


    'SETTER COBROS

    <WebMethod()> _
    Public Function setFacturasCobros(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

        Dim retVal As String = ""

        Try


            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            retVal = oSubida.setFacturasCobro(Empleado, Password, strJson)

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
        End Try

        Return retVal

    End Function

    <WebMethod()> _
    Public Function setLocalizacionDireccionClientes(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

        Dim retVal As String = ""

        Try


            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            retVal = oSubida.setLocalizacionDireccionClientes(Empleado, strJson)

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
        End Try

        Return retVal

    End Function

    <WebMethod()> _
    Public Function setLocalizacionesEmpleado(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

        Dim retVal As String = ""

        Try


            If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
                Return "Empleado incorrecto"
            End If

            Dim oSubida As New clsDatosSubida(Me)
            retVal = oSubida.setLocalizacionesEmpleado(Empleado, strJson)

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            retVal = ex.Message
        End Try

        Return retVal

    End Function

    '<WebMethod()> _
    'Public Function setMensajeSalida(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

    '    Dim retVal As String = ""

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        If checkLoginSAP(Empleado) = False Then
    '            clsLog.Log.Fatal("No esta autenticado en SAP" & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '            Return "No esta autenticado en SAP"
    '        End If


    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setMensajeSalida(Empleado, strJson)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try

    '    Return retVal

    'End Function

    '<WebMethod()> _
    'Public Function setMensajeLeido(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJson As String) As String

    '    Dim retVal As String = ""

    '    Try


    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setMensajeLeido(Empleado, strJson)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try

    '    Return retVal

    'End Function

    '<WebMethod()> _
    'Public Function setTesoreria(ByVal Empleado As Integer, ByVal Password As String, ByVal Version As Integer, strJsonCab As String, strJsonLin As String) As String

    '    Dim retVal As String = ""

    '    Try

    '        If Not New clsDatosBajada().getEmpleadoCorrecto(Empleado, Password, Version) Then
    '            Return "Empleado incorrecto"
    '        End If

    '        Dim oSubida As New clsDatosSubida(Me)
    '        retVal = oSubida.setTesoreria(Empleado, strJsonCab, strJsonLin)

    '    Catch ex As Exception
    '        clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
    '        Return ex.Message
    '    End Try

    '    Return retVal

    'End Function


    'PRIVADAS

    Private Function checkLoginSAP(ByVal Empleado As Integer) As Boolean

        Dim oCon As SAPbobsCOM.Company = getSessionCompany(Empleado)
        If oCon Is Nothing Then
            If LogInSAP(Empleado) = False Then
                Return False
            End If
        ElseIf oCon.Connected = False Then
            If LogInSAP(Empleado) = False Then
                Return False
            End If
        End If

        Return True

    End Function



    <WebMethod(EnableSession:=True)> _
    Private Function getSessionCompany(ByVal Empleado As Integer) As SAPbobsCOM.Company

        Dim applicationObjectName As String = "ObjetoConexionSAP" & Empleado.ToString

        Try


            If IsNothing(Application.Item(applicationObjectName)) Then
                Dim oCon As New SAPbobsCOM.Company
                Application.Add(applicationObjectName, oCon)
                Return oCon
            Else
                Return CType(Application.Item(applicationObjectName), SAPbobsCOM.Company)
            End If

        Catch ex As Exception
            clsLog.Log.Fatal("Imposible conectar a la Company SAP" & " - " & applicationObjectName)
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

        Return Nothing

    End Function

    ''' <summary>
    ''' Saca al log el numero de variables de session (Com_object, company de SAP) acumuladas. 1 por usuario. 
    ''' </summary>
    ''' <remarks>
    ''' Desde el cambio de nombre de variable
    ''' application, cada usuario acumula su propia Company en una variable de aplicacion. Si hay que revertir estos cambios se deberia 
    ''' hacer cambiando la funcion getSessionCompany, no pasando el parametro de usuario para que el nombre de la variable sea
    ''' el mismo para todos. Es util para las transacciones porque no se pisan unas a otras pero hay que ver el rendimiento.
    ''' </remarks>
    ''' 

    <WebMethod()> _
    Public Sub ActiveAppsToLog()
        Try
            For i As Integer = 0 To Application.Count - 1
                clsLog.Log.Info("#" & i & " - " & Application.Item(i).GetType().Name)
            Next
        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub

    <WebMethod(EnableSession:=True)> _
    Private Function LogInSAP(ByVal Empleado As Integer) As Boolean

        Try
            Try

                Dim oCon As SAPbobsCOM.Company = getSessionCompany(Empleado)

                If Not IsNothing(oCon) Then
                    If oCon.Connected = True Then
                        Return True
                    End If
                End If

                Dim BD As String = ConfigurationManager.AppSettings.Get("bd").ToString
                Dim Server As String = ConfigurationManager.AppSettings.Get("server").ToString

                Dim SAPUser As String = ConfigurationManager.AppSettings.Get("userSAP").ToString()
                Dim SAPPass As String = ConfigurationManager.AppSettings.Get("passSAP").ToString()
                Dim License As String = ConfigurationManager.AppSettings.Get("licenseServer").ToString

                Dim DBType As Integer = CInt(ConfigurationManager.AppSettings.Get("DBType").ToString)
                Dim DBUser As String = ConfigurationManager.AppSettings.Get("DBUser").ToString
                Dim DBPass As String = ConfigurationManager.AppSettings.Get("DBPass").ToString()



                oCon.CompanyDB = BD
                oCon.UserName = SAPUser
                oCon.Password = SAPPass
                oCon.LicenseServer = License
                oCon.Server = Server

                oCon.DbUserName = DBUser
                oCon.DbPassword = DBPass
                oCon.DbServerType = CType(DBType, SAPbobsCOM.BoDataServerTypes)

                'Spanish 23, Portugues 19
                oCon.language = CType(ConfigurationManager.AppSettings.Get("Language").ToString(), SAPbobsCOM.BoSuppLangs)
                oCon.UseTrusted = False

                Dim iEstado As Integer = oCon.Connect
                Dim err As String = oCon.GetLastErrorDescription

                If iEstado = 0 Then
                    Return True
                Else
                    clsLog.Log.Fatal("Estado de SAP.Connect es: " & iEstado & ":" & err & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                    Return False
                End If

            Catch ex As Exception
                clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
                Return False
            End Try

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
            Return False
        End Try
    End Function

End Class